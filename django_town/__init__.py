__author__ = 'Juyoung Lee'
__license__ = "MIT"
__version__ = "0.4.0"
__email__ = "lee.juyoung04@gmail.com"


def get_version():
    """return current django_town version.
    """
    return __version__
