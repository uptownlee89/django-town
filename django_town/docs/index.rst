.. Django Town documentation master file, created by
sphinx-quickstart on Sat Feb 22 14:59:25 2014.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Django Town documentation!
==========================

Contents:

.. automodule:: django_town
    :members:

.. toctree::
    :maxdepth: 2
        cache
        rest
        oauth2



.. todo::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

