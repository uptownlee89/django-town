Rest
====

View
----

.. automodule:: django_town.rest.views
    :members:

Permissions
-----------

.. automodule:: django_town.rest.permissions
    :members:

Exceptions
----------

.. automodule:: django_town.rest.exceptions
    :members:

Resources
---------

.. automodule:: django_town.rest.resources.base
    :members:

.. automodule:: django_town.rest.resources.model_resource
    :members:

.. automodule:: django_town.rest.resources.mongo_resource
    :members:

.. automodule:: django_town.rest.resources.fields
    :members:

.. automodule:: django_town.rest.serializers
    :members:


Manager
-------

.. automodule:: django_town.rest.manager
    :members:

