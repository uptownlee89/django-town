# -*- encoding: utf-8 -*-
# import cStringIO
from __future__ import division
from django.db.models import signals
from django.utils.six import BytesIO

from PIL import Image
from django.core.files.base import ContentFile
from django.db.models.fields.files import ImageFieldFile
from django_town.utils.with3 import urlparse


def generate_thumb(img, thumb_size, extension, image_size=None, quality=50):
    img.seek(0)  # see http://code.djangoproject.com/ticket/8222 for details
    image = Image.open(img)

    # Convert to RGB if necessary
    if image.mode not in ('L', 'RGB', 'RGBA'):
        image = image.convert('RGB')
    # get size
    thumb_w, thumb_h = thumb_size
    if not thumb_h:
        if not image_size:
            image_size = image.size
        xsize, ysize = image_size
        thumb_h = float(thumb_w) / xsize * ysize
    elif not thumb_w:
        if not image_size:
            image_size = image.size
        xsize, ysize = image_size
        thumb_w = float(thumb_h) / ysize * xsize
    # If you want to generate a square thumbnail
    if thumb_w == thumb_h:
        # quad
        if not image_size:
            image_size = image.size
        xsize, ysize = image_size
        # get minimum size
        minsize = min(xsize, ysize)
        # largest square possible in the image
        xnewsize = (xsize - minsize) // 2
        ynewsize = (ysize - minsize) // 2
        # crop it
        image2 = image.crop((xnewsize, ynewsize, xsize - xnewsize, ysize - ynewsize))
        # load is necessary after crop
        image2.load()
        # thumbnail of the cropped image (with ANTIALIAS to make it look better)
        image2.thumbnail((thumb_w, thumb_h), Image.ANTIALIAS)
    else:
        # not quad
        image2 = image
        image2.thumbnail((thumb_w, thumb_h), Image.ANTIALIAS)

    io = BytesIO()
    # PNG and GIF are the same, JPG is JPEG
    if extension.upper() == 'JPG':
        extension = 'JPEG'
    image2.save(io, extension, optimize=True, quality=quality)
    return ContentFile(io.getvalue()), (thumb_w, thumb_h)



def get_thumbnails(url, original_size, sizes):
    ret = []
    prefix = ''
    if '/' in url:
        prefix, url = url.rsplit('/', 1)
    for size in sizes:
        (w, h) = size
        split = url.rsplit('.', 1)
        if len(split) == 1:
            split.append('jpg')
        thumb_w, thumb_h = size
        if not thumb_h:
            xsize, ysize = original_size
            thumb_h = float(thumb_w) / xsize * ysize
        elif not thumb_w:
            xsize, ysize = original_size
            thumb_w = float(thumb_h) / ysize * xsize
        thumb_name = '%s.%sx%s.%s' % (split[0], w or '', h or '', split[1])
        ret.append({'url': prefix + '/' + thumb_name, 'wight': int(thumb_w), 'height': int(thumb_h)})
    sorted(ret, key=lambda k: k['wight'])
    return ret


class ThumbnailFile(ImageFieldFile):
    def __init__(self, *args, **kwargs):
        super(ThumbnailFile, self).__init__(*args, **kwargs)
        self.sizes = self.field.sizes
        self.domain = self.field.domain

    def save(self, name, content, save=True):
        content.seek(0)
        super(ThumbnailFile, self).save(name, content, save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = self.name.rsplit('.', 1)
                if len(split) == 1:
                    split.append('jpg')
                thumb_content, size_unused = generate_thumb(content, (w, h), split[1],
                                                            image_size=(self.width, self.height))
                thumb_name = '%s.%sx%s.%s' % (split[0], w or '', h or '', split[1])
                thumb_name_ = self.storage.save(thumb_name, thumb_content)
                if not thumb_name == thumb_name_:
                    raise ValueError('There is already a file named %s' % thumb_name)

    def delete(self, save=True):
        name = self.name
        super(ThumbnailFile, self).delete(save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = name.rsplit('.', 1)
                if len(split) == 1:
                    split.append('jpg')
                thumb_name = '%s.%sx%s.%s' % (split[0], w or '', h or '', split[1])
                self.storage.delete(thumb_name)

    def get_thumbnails(self, url=None):
        ret = []
        if self.domain:
            parsed_uri = urlparse.urlparse(self.domain)
        else:
            if not url:
                url = self.url
            parsed_uri = urlparse.urlparse(url)
        domain = "%s://%s" % (parsed_uri.scheme, parsed_uri.netloc)
        path = parsed_uri.path
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = path.rsplit('.', 1)
                if len(split) == 1:
                    split.append('jpg')
                thumb_w, thumb_h = size
                if not thumb_h:
                    xsize, ysize = self.width, self.height
                    thumb_h = float(thumb_w) / xsize * ysize
                elif not thumb_w:
                    xsize, ysize = self.width, self.height
                    thumb_w = float(thumb_h) / ysize * xsize
                thumb_name = '%s%s.%sx%s.%s' % (domain, split[0], w or '', h or '', split[1])
                ret.append({'source': thumb_name, 'wight': int(thumb_w), 'height': int(thumb_h)})
        sorted(ret, key=lambda k: k['wight'])
        return ret



class MultiSourceImageFile(ImageFieldFile):
    def __init__(self, *args, **kwargs):
        super(MultiSourceImageFile, self).__init__(*args, **kwargs)
        # self.field
        # self.instance
        self.sources_field = self.field.sources_field
        self.sizes = getattr(self.instance, self.sources_field)
        self.domain = self.field.domain

    def get_image_for_size(self, size, url=None):
        if self.domain:
            parsed_uri = urlparse.urlparse(self.domain)
        else:
            if not url:
                url = self.name
            parsed_uri = urlparse.urlparse(url)
        domain = "%s://%s" % (parsed_uri.scheme, parsed_uri.netloc)
        path = parsed_uri.path
        (w, h) = size
        split = path.rsplit('.', 1)
        if len(split) == 1:
            split.append('jpg')
        if not size in self.sizes:
            self.sizes.append(size)
            content = self
            self.open()
            thumb_content, size_unused = generate_thumb(content, (w, h), split[1],
                                            image_size=(self.width, self.height))
            thumb_name = '%s.%sx%s.%s' % (split[0], w or '', h or '', split[1])
            thumb_name_ = self.storage.save(thumb_name, thumb_content)
            if not thumb_name == thumb_name_:
                raise ValueError('There is already a file named %s' % thumb_name)
            setattr(self.instance, self.sources_field, self.sizes)
            self.instance.save()
        thumb_w, thumb_h = size
        if not thumb_h:
            xsize, ysize = self.width, self.height
            thumb_h = float(thumb_w) / xsize * ysize
        elif not thumb_w:
            xsize, ysize = self.width, self.height
            thumb_w = float(thumb_h) / ysize * xsize
        image_source = '%s%s.%sx%s.%s' % (domain, split[0], w or '', h or '', split[1])
        return {'source': image_source, 'wight': int(thumb_w), 'height': int(thumb_h)}


    def save(self, name, content, save=True):
        content.seek(0)
        super(MultiSourceImageFile, self).save(name, content, save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = self.name.rsplit('.', 1)
                if len(split) == 1:
                    split.append('jpg')
                thumb_content, size_unused = generate_thumb(content, (w, h), split[1],
                                                            image_size=(self.width, self.height))
                thumb_name = '%s.%sx%s.%s' % (split[0], w or '', h or '', split[1])
                thumb_name_ = self.storage.save(thumb_name, thumb_content)
                if not thumb_name == thumb_name_:
                    raise ValueError('There is already a file named %s' % thumb_name)

    def delete(self, save=True):
        name = self.name
        super(MultiSourceImageFile, self).delete(save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = name.rsplit('.', 1)
                if len(split) == 1:
                    split.append('jpg')
                thumb_name = '%s.%sx%s.%s' % (split[0], w or '', h or '', split[1])
                self.storage.delete(thumb_name)

    def get_all_sources(self, url=None):
        ret = []
        if not url:
            url = self.url
        parsed_uri = urlparse.urlparse(url)
        domain = "%s://%s" % (parsed_uri.scheme, parsed_uri.netloc)
        path = parsed_uri.path
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = path.rsplit('.', 1)
                if len(split) == 1:
                    split.append('jpg')
                thumb_w, thumb_h = size
                if not thumb_h:
                    xsize, ysize = self.width, self.height
                    thumb_h = float(thumb_w) / xsize * ysize
                elif not thumb_w:
                    xsize, ysize = self.width, self.height
                    thumb_w = float(thumb_h) / ysize * xsize
                thumb_name = '%s%s.%sx%s.%s' % (domain, split[0], w or '', h or '', split[1])
                ret.append({'source': thumb_name, 'wight': int(thumb_w), 'height': int(thumb_h)})
        sorted(ret, key=lambda k: k['wight'])
        return ret

    def deconstruct(self):
        name, path, args, kwargs = super(MultiSourceImageFile, self).deconstruct()
        if self.source_field:
            kwargs['source_field'] = self.source_field
        return name, path, args, kwargs

    def contribute_to_class(self, cls, name):
        super(MultiSourceImageFile, self).contribute_to_class(cls, name)
        # Attach update_dimension_fields so that dimension fields declared
        # after their corresponding image field don't stay cleared by
        # Model.__init__, see bug #11196.
        # Only run post-initialization dimension update on non-abstract models
        if not cls._meta.abstract:
            signals.post_init.connect(self.update_source_field, sender=cls)

    def update_source_field(self, instance, force=False, *args, **kwargs):
        if not self.source_field:
            return

        file = getattr(instance, self.attname)

        # Nothing to update if we have no file and not being forced to update.
        if not file and not force:
            return

        source_field_filled = not(
            (self.source_field and not getattr(instance, self.source_field))
        )
        # When both dimension fields have values, we are most likely loading
        # data from the database or updating an image field that already had
        # an image stored.  In the first case, we don't want to update the
        # dimension fields because we are already getting their values from the
        # database.  In the second case, we do want to update the dimensions
        # fields and will skip this return because force will be True since we
        # were called from ImageFileDescriptor.__set__.
        if source_field_filled and not force:
            return

        # Update the width and height fields.
        if self.source_field:
            setattr(instance, self.source_field, width)
