from django.core.management.base import BaseCommand
from django_town.oauth2.models import Client, Service

class Command(BaseCommand):

    def handle(self, *args, **options):
        client = Client.objects.get(name=args[0], service=Service.objects.get(name=args[1]))
        available_scope = client.available_scope
        if isinstance(available_scope, dict):
            available_scope = []
        available_scope.append(args[2])
        client.available_scope = available_scope
        client.save()
        print(client.available_scope)