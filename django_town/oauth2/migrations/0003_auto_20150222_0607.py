# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('oauth2', '0002_auto_20150102_1318'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='userclientsecretkey',
            unique_together=set([('user_id', 'client')]),
        ),
    ]
