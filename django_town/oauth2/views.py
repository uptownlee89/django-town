#-*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext

from django_town.oauth2 import get_credential, authorization, OAuth2Error
from django_town.helper.http import http_json_response
from django_town.utils.rand import generate_random_from_vschar_set
from django_town.core.settings import OAUTH2_SETTINGS


class TokenView(View):
    """
    https://www.example.com/oauth2/token?client_id=<xxx>&client_secret=<xxxx>&grant_type=<grant_type>&scope=[scope][&code=<code>]
    grant_type은 현재
    """

    @csrf_exempt
    def post(self, request):
        try:
            return http_json_response(get_credential(request).to_dict())
        except OAuth2Error as e:
            return http_json_response(e.to_dict(), status=400)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(TokenView, self).dispatch(*args, **kwargs)


class AuthorizeView(View):

    @csrf_exempt
    def post(self, request):
        return HttpResponseRedirect(authorization(request))

    @csrf_exempt
    def get(self, request):
        return render_to_response("oauth2/authorize.html", {'get': request.GET,
                                                            'state': generate_random_from_vschar_set(),
                                                            'action': OAUTH2_SETTINGS.BASE_URL + "/token"
        }, RequestContext(request))

    @method_decorator(csrf_protect)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthorizeView, self).dispatch(request, *args, **kwargs)


class DialogView(View):

    @csrf_exempt
    def get(self, request):
        return render_to_response("oauth2/dialog.html", request.GET, RequestContext(request))

class CheckPermissionView(View):

    @csrf_exempt
    def get(self, request):
        return HttpResponseRedirect(authorization(request))