from django.apps import AppConfig
from django_town.rest.manager import rest_manager

# django 1.7부터 추가됨
class RestConfig(AppConfig):
    name = 'django_town.rest'

    def ready(self):
        rest_manager.setup()