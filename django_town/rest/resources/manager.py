from django.db.models import Model
from mongoengine import Document



class ResourceManager(object):

    _resource_lookup = {}

    def set_default_resource(self, resource, model_or_document):
        if issubclass(model_or_document, Document):
            self._resource_lookup[model_or_document] = resource
        elif issubclass(model_or_document, Model):
            self._resource_lookup[model_or_document] = resource

    def get_resource(self, model_or_document):
        return



#
# _resource_lookup = {}
#
#
# def resource_by_name(name):
#     return _resource_lookup[name]
#
#
# class ResourceCacheManager(object):
#
#     def __init__(self):
#         self.lookup = {}
#
#     def register(self, trigger_resource, resource, mapping_role):
#         if isinstance(resource, six.string_types):
#             resource = resource_by_name(resource)
#         if isinstance(trigger_resource, six.string_types):
#             trigger_resource = resource_by_name(trigger_resource)
#         self.lookup[trigger_resource.__name__] = [resource, mapping_role]
#         # print(self.lookup)
#
#     def check_and_execute(self, resource_instance, action):
#         if resource_instance._manager.__name__ in self.lookup:
#             target, role = self.lookup[resource_instance._manager.__name__]
#             kwargs = {}
#             for key, val in six.iteritems(role):
#                 kwargs[val] = getattr(resource_instance, key)
#             target().invalidate_cache(**kwargs)
#
#
# resource_cache_manager = ResourceCacheManager()

resource_manager = ResourceManager()

