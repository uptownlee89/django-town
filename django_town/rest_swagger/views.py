from django.db import models
from django.utils.six import string_types

from django_town.rest import RestApiView, rest_api_manager, MongoResource, ModelResource
from django_town.helper.http import http_json_response
from django_town.oauth2.swagger import swagger_authorizations_data
from django_town.social.oauth2.permissions import OAuth2Authenticated


class ApiDocsView(RestApiView):
    def read(self, request, api_version):
        def load_cache(api_version="alpha"):
            manager = rest_api_manager(api_version)
            method_lookup = {'create': 'POST', 'read': 'GET', 'delete': 'DELETE', 'update': 'PUT'}
            ret = {'info': {'title': manager.name,
                   'description': manager.description,
                            },
                   'apiVersion': manager.api_version, 'swaggerVersion': "1.2", 'basePath': manager.base_url,
                   'resourcePath': manager.base_url,
                   'authorizations': swagger_authorizations_data()}
            apis = []
            object_types = {
                "Error": {
                    "id": "Error",
                    "required": ['error'],
                    "properties": {
                        "error": {
                            "type": "string"
                        },
                        "field": {
                            "type": "string"
                        },
                        "message": {
                            "type": "string"
                        },
                        "resource": {
                            "type": "string"
                        }
                    }
                }
            }
            for view_cls in manager.api_list:
                global_params = []
                path = view_cls.path()
                if path == "":
                    continue
                if '{}' in path:
                    path = path.replace('{}', '{pk}')
                    global_params.append(
                        {
                            "paramType": "path",
                            "name": 'pk',
                            "description": 'primary key for object',
                            "dataType": 'string',
                            # "format": 'int64',
                            "required": True,
                        }
                    )
                responseMessages = [
                    {
                        'code': 500,
                        "message": "internal_error",
                        "responseModel": "Error"
                    },
                    {
                        'code': 409,
                        "message": "method_not_allowed, conflict",
                        "responseModel": "Error"
                    },
                    {
                        'code': 404,
                        "message": "not_found",
                        "responseModel": "Error"
                    },
                    # {
                    #     'code': 409,
                    #     "message": "conflict",
                    #     "responseModel": "Error"
                    # },
                    {
                        'code': 403,
                        "message": "forbidden",
                        "responseModel": "Error"
                    },
                    {
                        'code': 401,
                        "message": "permission_denied, unauthorized",
                        "responseModel": "Error"
                    },
                    # {
                    #     'code': 401,
                    #     "message": "unauthorized",
                    #     "responseModel": "Error"
                    # },
                    {
                        'code': 400,
                        "message": "form_invalid, form_required, bad_request",
                        "responseModel": "Error"
                    },
                    # {
                    #     'code': 400,
                    #     "message": "form_required",
                    #     "responseModel": "Error"
                    # },
                    # {
                    #     'code': 400,
                    #     "message": "bad_request",
                    #     "responseModel": "Error"
                    # },
                ]
                current_api = {
                    'path': path,
                    'description': view_cls.__doc__,
                }
                operations = []
                api_info = getattr(view_cls, 'api_info', {})
                for cur_method in view_cls.crud_method_names:
                    cur_method = cur_method.lower()
                    if not hasattr(view_cls, cur_method):
                        continue
                    op = {
                        'method': method_lookup[cur_method],
                        'parameters': global_params.copy(),
                        'responseMessages': responseMessages,
                        'nickname': api_info.get(cur_method, {}).get('name', None) or cur_method + ' ' + path,
                        'summary': api_info.get(cur_method, {}).get('summary', ""),
                        'note': api_info.get(cur_method, {}).get('note', "")
                    }
                    params = op['parameters']
                    need_resource_check = True
                    for each_attribute in dir(view_cls):
                        # print(cur_method, view_cls)
                        param_type = 'query' if cur_method == 'read' else 'form'
                        if (cur_method + '_parameters') in each_attribute and \
                            isinstance(getattr(view_cls, each_attribute), (tuple, list)):
                            need_resource_check = False
                            info_dict = {}
                            if hasattr(view_cls, each_attribute + "_info"):
                                info_dict = getattr(view_cls, each_attribute + "_info")
                            is_safe = 'safe' in each_attribute
                            for each in getattr(view_cls, each_attribute):
                                description = ""
                                required = is_safe
                                dataType = "string"
                                if isinstance(each, (tuple, list)):
                                    data = info_dict.get(each[0])
                                else:
                                    data = info_dict.get(each)
                                if data:
                                    if isinstance(data, string_types):
                                        description = data
                                    else:
                                        if data.get("description"):
                                            description = data.get("description")
                                        if data.get("required"):
                                            required = required or data.get("required")
                                        if data.get("type"):
                                            dataType = data.get("type")

                                if isinstance(each, (tuple, list)):
                                    allowMultiple = False
                                    if '[]' in each[0]:
                                        allowMultiple = True
                                        each[0] = each[0][:-2]
                                    if each[1] == int:
                                        params.append(
                                            {
                                                "paramType": param_type,
                                                "name": each[0],
                                                "type": 'int',
                                                "required": required,
                                                "description": description,
                                                "allowMultiple": allowMultiple
                                            }
                                        )
                                    elif each[1] == float:
                                        params.append(
                                            {
                                                "paramType": param_type,
                                                "name": each[0],
                                                "type": 'float',
                                                "required": required,
                                                "description": description,
                                                "allowMultiple": allowMultiple
                                            }
                                        )
                                    else:
                                        params.append(
                                            {
                                                "paramType": param_type,
                                                "name": each[0],
                                                "type": 'string',
                                                "required": required,
                                                "description": description,
                                                "allowMultiple": allowMultiple
                                            }
                                        )
                                else:
                                    allowMultiple = False
                                    if '[]' in each:
                                        allowMultiple = True
                                        each = each[:-2]
                                    params.append(
                                        {
                                            "paramType": param_type,
                                            "name": each,
                                            "description": description,
                                            "type": dataType,
                                            "required": required,
                                            "allowMultiple": allowMultiple
                                        }
                                    )
                    if need_resource_check and hasattr(view_cls, 'resource'):
                        resource = view_cls.resource
                        if cur_method == "create":
                            create_required = resource._meta.create_required if resource._meta.create_required else []
                            if resource._meta.create_acceptable:
                                if isinstance(resource, ModelResource):
                                    fields = resource._meta.model._meta.fields
                                    for each_fields in fields:
                                        if each_fields in resource._meta.create_acceptable:
                                            allowMultiple = False
                                            if isinstance(each_fields, models.IntegerField):
                                                dataType = "int"
                                            elif isinstance(each_fields, models.AutoField):
                                                continue
                                            elif isinstance(each_fields, models.ForeignKey):
                                                dataType = "int"
                                            elif isinstance(each_fields, models.FloatField):
                                                dataType = "float"
                                            else:
                                                dataType = "string"
                                            description = str(getattr(each_fields, 'description'))
                                            required = getattr(each_fields, 'required', False) \
                                                       or each_fields in create_required

                                            params.append(
                                                {
                                                    "paramType": "form",
                                                    "name": each_fields.name,
                                                    "description": description,
                                                    "type": dataType,
                                                    "required": required,
                                                    "allowMultiple": allowMultiple
                                                }
                                            )
                                elif isinstance(resource, MongoResource):
                                    document = resource._meta.document
                            else:
                                fields = resource._meta.model._meta.fields
                                for each_fields in fields:
                                    allowMultiple = False
                                    if (hasattr(each_fields, 'editable') and not each_fields.editable) \
                                            or getattr(each_fields, 'auto_now', False) \
                                            or getattr(each_fields, 'auto_now_add', False):
                                        continue
                                    if isinstance(each_fields, models.IntegerField):
                                        dataType = "int"
                                    elif isinstance(each_fields, models.AutoField):
                                        continue
                                    elif isinstance(each_fields, models.ForeignKey):
                                        dataType = "int"
                                    elif isinstance(each_fields, models.FloatField):
                                        dataType = "float"
                                    elif isinstance(each_fields, models.FileField):
                                        dataType = "file"
                                    else:
                                        dataType = "string"
                                    description = str(getattr(each_fields, 'description'))
                                    required = getattr(each_fields, 'required', False) or each_fields in create_required

                                    params.append(
                                        {
                                            "paramType": "query",
                                            "name": each_fields.name,
                                            "description": description,
                                            "type": dataType,
                                            "required": required,
                                            "allowMultiple": allowMultiple
                                        }
                                    )

                            if resource._meta.create_required:
                                pass
                            # if resource._meta.create_acceptable:
                            #     for each in resource._meta.create_acceptable:
                            #         params.append(
                            #             {
                            #                 "paramType": "query",
                            #                 "name": each,
                            #                 "description": description,
                            #                 "type": "string",
                            #                 "format": format_,
                            #                 "required": required,
                            #             }
                            #         )
                            #         pass
                            # if resource._meta.create_required:
                            #     for each in resource._meta.create_required:
                            #         pass
                            #
                            # create_acceptable = None
                            # create_required = None
                            # create_exclude = None
                            #
                            # update_acceptable = None
                            # update_required = None
                            # update_exclude = None
                            pass
                    for each_permission in view_cls.permission_classes:
                        if issubclass(each_permission, OAuth2Authenticated):
                            op['authorizations'] = {
                                "oauth2": [
                                    {
                                        # "scope": "user_email",
                                        "scope": [],
                                        "description": ""
                                    },
                                ]
                            }

                    operations.append(op)
                current_api['operations'] = operations
                apis.append(current_api)

            ret['apis'] = apis
            ret["models"] = object_types
            return ret

        # ret = SimpleCache(key_format="api-doc:%(api_version)s", duration=60 * 60 * 24,
        #                   load_callback=load_cache).get(api_version=api_version)
        response = http_json_response(load_cache(api_version=api_version))
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "*"
        return response
