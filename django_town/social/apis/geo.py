from django_town.rest import RestDocumentApiView, RestQuerySetCollectionApiView
from django_town.social.resources.geo import default_geo_resource
from django_town.social.models.geo import GeoNameLookup
# from django_town.social.define.place_categories import PLACE_CATEGORIES


class GeosApiView(RestQuerySetCollectionApiView):
    resource = default_geo_resource
    crud_method_names = ['read']
    max_limit = 50
    read_parameters = ['lang']

    def query_set(self, **kwargs):
        # print(kwargs)
        if not kwargs['parameters']['q'] or kwargs['parameters']['q'] == '':
            return []
        return GeoNameLookup.objects.filter(name__startswith=kwargs['parameters']['q']).values('geoname_id')

    def collection_step1(self, start, limit, request=None, parameters=None, **kwargs):
        return list([each['geoname_id'] for each in self.query_set(parameters=parameters, request=request,
                                   **kwargs)[start:start + limit]])

    def collection_step2(self, list_with_extra, start, limit, parameters=None, fields=None, options=None, request=None,
                         **kwargs):
        lang = parameters.get('lang')
        id_set = set()
        ret = []
        for each in list_with_extra[:limit]:
            if each not in id_set:
                id_set.add(each)
                ret.append(self.resource(each).to_dict(fields=fields, options=options, request=request))
        for each in ret:
            if lang in each['alter_names']:
                each['name'] = each['alter_names'][lang]
            del each['alter_names']
        return ret