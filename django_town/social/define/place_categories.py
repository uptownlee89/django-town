PLACE_CATEGORIES = (
    (0, "undefined"),
    (1000, "accounting"),
    (1100, "airport"),
    (1200, "amusement_park"),
    (1300, "aquarium"),
    (1400, "art_gallery"),
    (1500, "atm"),
    (1600, "bakery"),
    (1700, "bank"),
    (1800, "bar"),
    (1900, "beauty_salon"),
    (2000, "bicycle_store"),
    (2100, "book_store"),
    (2200, "bowling_alley"),
    (2300, "bus_station"),
    (2400, "cafe"),
    (2500, "campground"),
    (2600, "car_dealer"),
    (2700, "car_rental"),
    (2800, "car_repair"),
    (2900, "car_wash"),
    (3000, "casino"),
    (3100, "cemetery"),
    (3200, "church"),
    (3300, "city_hall"),
    (3400, "clothing_store"),
    (3500, "convenience_store"),
    (3600, "courthouse"),
    (3700, "dentist"),
    (3800, "department_store"),
    (3900, "doctor"),
    (4000, "electrician"),
    (4100, "electronics_store"),
    (4200, "embassy"),
    (4300, "establishment"),
    (4400, "finance"),
    (4500, "fire_station"),
    (4600, "florist"),
    (4700, "food"),
    (4800, "funeral_home"),
    (4900, "furniture_store"),
    (5000, "gas_station"),
    (5100, "general_contractor"),
    (5200, "grocery_or_supermarket"),
    (5300, "gym"),
    (5400, "hair_care"),
    (5500, "hardware_store"),
    (5600, "health"),
    (5700, "hindu_temple"),
    (5800, "home_goods_store"),
    (5900, "hospital"),
    (6000, "insurance_agency"),
    (6100, "jewelry_store"),
    (6200, "laundry"),
    (6300, "lawyer"),
    (6400, "library"),
    (6500, "liquor_store"),
    (6600, "local_government_office"),
    (6700, "locksmith"),
    (6800, "lodging"),
    (6900, "meal_delivery"),
    (7000, "meal_takeaway"),
    (7100, "mosque"),
    (7800, "movie_rental"),
    (7900, "movie_theater"),
    (8000, "moving_company"),
    (8100, "museum"),
    (8200, "night_club"),
    (8300, "painter"),
    (8400, "park"),
    (8500, "parking"),
    (8600, "pet_store"),
    (8700, "pharmacy"),
    (8800, "physiotherapist"),
    (8900, "place_of_worship"),
    (9000, "plumber"),
    (9100, "police"),
    (9200, "post_office"),
    (9300, "real_estate_agency"),
    (9400, "restaurant"),
    (9500, "roofing_contractor"),
    (9600, "rv_park"),
    (9700, "school"),
    (9800, "shoe_store"),
    (9900, "shopping_mall"),
    (10000, "spa"),
    (10100, "stadium"),
    (10200, "storage"),
    (10300, "store"),
    (10400, "subway_station"),
    (10500, "synagogue"),
    (10600, "taxi_stand"),
    (10700, "train_station"),
    (10800, "travel_agency"),
    (10900, "university"),
    (11000, "veterinary_care"),
    (11100, "zoo"),




    (70000, "locality"),

)

CONTACT_TYPES = (
    (0, "phone"),
    (1, "email"),
    (2, "homepage"),
    (3, "facebook"),
    (4, "twitter"),
)

ADDRESS_COMPONENT_TYPES = (
    (0, "country"),
    (1, "administrative_area_level_1"),
    (2, "administrative_area_level_2"),
    (3, "administrative_area_level_3"),
    (4, "colloquial_area"),
    (5, "locality"),
    (6, "sublocality"),
    (7, "route"),
    (8, "intersection"),
    (9, "neighborhood"),
    (10, "premise"),
    (11, "natural_feature"),
    (12, "airport"),
    (13, "park"),
    (14, "point_of_interest"),
    (15, "post_box"),
    (16, "street_number"),
    (17, "street_address"),
    (18, "floor"),
    (19, "room"),
)


# AREA_TYPES = (
#     (0, 'ppl'),
#     (1, 'adm4'),
#     (2, 'adm3'),
#     (3, 'ppla3'),
#     (4, 'admd'),
#     (5, 'adm2'),
#     (6, 'ppla2'),
#     (7, 'ppla4'),
#     (8, 'ppll'),
#     (9, 'pplx'),
#     (10, 'ppla'),
#     (11, 'adm1'),
#     (12, 'pplc'),
#     (13, 'pcli'),
#     (14, 'pplf'),
#     (15, 'pplq'),
#     (16, 'prsh'),
#     (17, 'adm5'),
#     (18, 'pplr'),
#     (19, 'pcld'),
#     (20, 'ppls'),
#     (21, 'pclix'),
#     (22, 'pplg'),
#     (23, 'pcls'),
#     (24, 'pcl'),
#     (25, 'pclf'),
#     (26, 'terr'),
#     (27, 'lter'),
#     (28, 'stlmt'),
# # {'PPL': 303058, 'ADM4': 48427, 'ADM3': 27280, 'PPLA3': 18432,
# # 'ADMD': 17749, 'ADM2': 17398, 'PPLA2': 10753, 'PPLA4': 6879,
# # 'PPLL': 4621, 'PPLX': 4374, 'PPLA': 3389, 'ADM1': 2584,
# # 'PPLC': 238, 'PCLI': 193, 'PPLF': 191, 'PPLQ': 112, 'PRSH': 67,
# # 'ADM5': 66, 'PPLR': 58, 'PCLD': 32, 'PPLS': 19, 'PCLIX': 7,
# # 'PPLG': 6, 'PCLS': 5, '': 3, 'PCL': 3, 'PCLF': 3, 'TERR': 3,
# # 'LTER': 1, 'STLMT': 1})
# )