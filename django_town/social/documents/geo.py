import mongoengine
import datetime

from django_town.mongoengine_extension import OptionField, ResourceField
from django_town.social.define import PLACE_CATEGORIES, CONTACT_TYPES
from django_town.social.documents.thing import Thing
from django_town.social.resources.region import RegionResource


class GeoNamePointLookup(mongoengine.Document):
    geoname_id = mongoengine.IntField()
    type = mongoengine.StringField()
    location = mongoengine.PointField()

    meta = {
        'indexes': [{'fields': ['location', 'type', 'geoname_id']}],
    }