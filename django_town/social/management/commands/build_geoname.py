from django.core.management.base import BaseCommand
from django_town.social.models import Country, TimeZone, GeoName, GeoNameLookup
from django_town.social.models.geo import _GeoLookup
from django_town.social.documents.geo import GeoNamePointLookup
from django.db import transaction


class Command(BaseCommand):

    def handle(self, *args, **options):
        transaction.set_autocommit(True)
        # Country.objects.all().delete()
        # TimeZone.objects.all().delete()
        # GeoName.objects.all().delete()
        # _GeoLookup.objects.all().delete()
        # GeoNameLookup.objects.all().delete()
        # with transaction.atomic():
        with open('geodata/data_ci.txt') as f:
            for line in f:
                data = line.replace('\n', '').split('\t')
                Country(id=data[0], ascii_name=data[2], code=data[1]).save()

        with open('geodata/data_tz.txt') as f:
            for line in f:
                data = line.replace('\n', '').split('\t')
                TimeZone(id=data[0], name=data[1], country_id=(data[2] if data[2] != '0' else None),
                         gmt=data[3], dst=data[4], raw=data[5]).save()

#
# class GeoName(models.Model):
#     ascii_name = models.CharField(max_length=200)
#     type = models.CharField(db_index=True, max_length=10)
#     lat = models.FloatField()
#     lng = models.FloatField()
#     country = models.ForeignKey(Country, blank=True, null=True)
#     timezone = models.ForeignKey(TimeZone, blank=True, null=True)
#     pp = models.IntegerField(db_index=True)
#
#     class Meta:
#         app_label = 'social'
# geoname_id = mongoengine.IntField()
# location = mongoengine.PointField()

        cur = []
        cur2 = []
        cur3 = []
        with open('geodata/data.txt') as f:
            for line in f:
                data = line.replace('\n', '').split('\t')
                if data[7] == '0':
                    data[7] = None
                if data[9] == '0':
                    data[9] = None
                # print(data)
                cur.append(GeoName(id=data[0], ascii_name=data[2], lat=data[3], lng=data[4], type=data[6],
                            country_id=data[7], timezone_id=data[9],
                            pp=data[8]))
                cur2.append(_GeoLookup(geoname_id=data[0], gid=data[1], last_modified=data[10]))
                cur3.append(GeoNamePointLookup(location=[float(data[4]), float(data[3])], geoname_id=data[0]))


                if len(cur) > 3000:
                    GeoName.objects.bulk_create(cur)
                    _GeoLookup.objects.bulk_create(cur2)
                    GeoNamePointLookup.objects.insert(cur3)
                    cur = []
                    cur2 = []
                    cur3 = []
        GeoName.objects.bulk_create(cur)
        _GeoLookup.objects.bulk_create(cur2)
        GeoNamePointLookup.objects.insert(cur3)
        cur = []
        cur2 = []
        cur3 = []

        cur = []
        i = 0
        with open('geodata/data_lp.txt') as f:
            for line in f:
                data = line.replace('\n', '').split('\t')
                # print(str(data))

                if i in (9615, 9620, 28056, 48524, 58570, 71238, 122316, 135973,
                         136192, 153548, 203190, 236087, 271014, 291274, 310262, 326965, 331558, 347136, 362230,
                        381145, 446698, 517444, 528722, 555041, 559770, 606568, 654830, 656420
                ):
                    i += 1
                    continue
                i += 1
                cur.append(GeoNameLookup(geoname_id=data[0], lang=data[1], name=bytes(data[2], 'utf8').decode('utf8'), is_preferredName=data[3]))
                if len(cur) > 3000:
                    GeoNameLookup.objects.bulk_create(cur)
                    cur = []
        GeoNameLookup.objects.bulk_create(cur)
        cur = []
        transaction.set_autocommit(False)