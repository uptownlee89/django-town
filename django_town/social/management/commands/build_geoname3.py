from django.core.management.base import BaseCommand
from django_town.social.models import Country, TimeZone, GeoName, GeoNameLookup
from django_town.social.models.geo import _GeoLookup
from django_town.social.documents.geo import GeoNamePointLookup
from django.db import transaction


class Command(BaseCommand):

    def handle(self, *args, **options):
        transaction.set_autocommit(True)
        # Country.objects.all().delete()
        # TimeZone.objects.all().delete()
        # GeoName.objects.all().delete()
        # _GeoLookup.objects.all().delete()
        # GeoNameLookup.objects.all().delete()
        # with transaction.atomic():
        with open('geodata/data_hr.txt') as f:
            for line in f:
                data = line.replace('\n', '').split('\t')
                p = data[0]
                c = data[1]
                GeoName.objects.filter(pk=c).update(parent_id=p)



        transaction.set_autocommit(False)