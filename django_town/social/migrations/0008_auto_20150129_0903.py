# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0007_remove_addresscomponent_code'),
    ]

    operations = [
        migrations.CreateModel(
            name='AddressComponentLookup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('locale', models.CharField(max_length=5)),
                ('name', models.CharField(max_length=60)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='addresscomponentlookup',
            unique_together=set([('locale', 'name')]),
        ),
        migrations.RemoveField(
            model_name='addresscomponent',
            name='ascii_name',
        ),
        migrations.AlterField(
            model_name='addresscomponent',
            name='name',
            field=models.CharField(max_length=60),
            preserve_default=True,
        ),
    ]
