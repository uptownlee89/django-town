# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0008_auto_20150129_0903'),
    ]

    operations = [
        migrations.AddField(
            model_name='addresscomponentlookup',
            name='component',
            field=models.ForeignKey(to='social.AddressComponent', default=None),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='addresscomponentlookup',
            unique_together=set([('locale', 'component')]),
        ),
    ]
