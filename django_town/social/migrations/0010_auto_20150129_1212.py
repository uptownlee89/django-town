# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0009_auto_20150129_1019'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addresscomponentlookup',
            name='locale',
            field=models.CharField(max_length=5, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='addresscomponentlookup',
            unique_together=set([]),
        ),
    ]
