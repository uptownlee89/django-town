# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0011_auto_20150129_1215'),
    ]

    operations = [
        migrations.CreateModel(
            name='GoogleCloudMessaging',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('user_id', models.IntegerField(default=0)),
                ('client_id', models.IntegerField(default=0)),
                ('device_token', models.CharField(max_length=256)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('state', models.IntegerField(max_length=1, db_index=True, choices=[('N', 'New'), ('P', 'Pending')], default=0)),
                ('message', models.CharField(max_length=100)),
                ('additional_info', models.CharField(max_length=500)),
                ('badge_count', models.IntegerField(default=1)),
            ],
            options={
                'ordering': ('-id',),
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='device',
            old_name='device_version',
            new_name='build_version',
        ),
    ]
