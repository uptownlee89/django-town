# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0012_auto_20150206_0257'),
    ]

    operations = [
        migrations.CreateModel(
            name='_GeoLookup',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('gid', models.IntegerField(unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('ascii_name', models.CharField(max_length=200)),
                ('code', models.CharField(unique=True, max_length=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GeoName',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('ascii_name', models.CharField(max_length=200)),
                ('type', models.CharField(db_index=True, max_length=10)),
                ('lat', models.FloatField()),
                ('lng', models.FloatField()),
                ('pp', models.IntegerField(db_index=True)),
                ('country', models.ForeignKey(null=True, blank=True, to='social.Country')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GeoNameLookup',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('lang', models.CharField(db_index=True, max_length=4)),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('is_preferredName', models.BooleanField(db_index=True)),
                ('geoname', models.ForeignKey(to='social.GeoName')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TimeZone',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=40)),
                ('gmt', models.FloatField()),
                ('dst', models.FloatField()),
                ('raw', models.FloatField()),
                ('country', models.ForeignKey(null=True, blank=True, to='social.Country')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='geoname',
            name='timezone',
            field=models.ForeignKey(null=True, blank=True, to='social.TimeZone'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='_geolookup',
            name='geoname',
            field=models.ForeignKey(unique=True, to='social.GeoName'),
            preserve_default=True,
        ),
    ]
