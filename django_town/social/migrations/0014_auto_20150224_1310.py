# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0013_auto_20150224_1310'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geonamelookup',
            name='is_preferredName',
            field=models.BooleanField(default=False, db_index=True),
            preserve_default=True,
        ),
    ]
