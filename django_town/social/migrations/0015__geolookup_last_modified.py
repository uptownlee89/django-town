# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0014_auto_20150224_1310'),
    ]

    operations = [
        migrations.AddField(
            model_name='_geolookup',
            name='last_modified',
            field=models.DateField(default=None, db_index=True),
            preserve_default=False,
        ),
    ]
