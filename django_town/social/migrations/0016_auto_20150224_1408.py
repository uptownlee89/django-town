# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0015__geolookup_last_modified'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geonamelookup',
            name='lang',
            field=models.CharField(max_length=10, db_index=True),
            preserve_default=True,
        ),
    ]
