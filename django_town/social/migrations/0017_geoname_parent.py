# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0016_auto_20150224_1408'),
    ]

    operations = [
        migrations.AddField(
            model_name='geoname',
            name='parent',
            field=models.ForeignKey(default=None, to='social.GeoName', blank=True, null=True),
            preserve_default=True,
        ),
    ]
