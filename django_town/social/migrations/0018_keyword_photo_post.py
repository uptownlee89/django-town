# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings

import django_town.social.models.post
import django_town.helper.fields


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0017_geoname_parent'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('mime_type', models.CharField(max_length=100)),
                ('photo', django_town.helper.fields.ImageThumbsField(upload_to=django_town.social.models.post._photo_name, height_field='height', width_field='width')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('height', models.IntegerField(default=0, editable=False)),
                ('width', models.IntegerField(default=0, editable=False)),
                ('keywords', models.ManyToManyField(to='social.Keyword')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('title', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('status', models.SmallIntegerField(default=0, choices=[(0, 'draft'), (1, 'private'), (2, 'publish')], db_index=True)),
                ('excerpt', models.TextField(default='')),
                ('content', models.TextField(default='')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('keywords', models.ManyToManyField(to='social.Keyword')),
                ('photos', models.ManyToManyField(to='social.Photo')),
                ('thumbnail', models.ForeignKey(to='social.Photo', blank=True, related_name='thumbnail', default=None)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
