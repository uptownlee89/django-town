# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0018_keyword_photo_post'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='mime_type',
        ),
    ]
