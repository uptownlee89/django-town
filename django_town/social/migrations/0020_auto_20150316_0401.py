# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

import django_town.social.models.post
import django_town.helper.fields


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0019_remove_photo_mime_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='sources',
            field=django_town.helper.fields.JSONField(default='{}'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='photo',
            name='photo',
            field=django_town.helper.fields.MultiSourceImageField(width_field='width', height_field='height', upload_to=django_town.social.models.post._photo_name),
            preserve_default=True,
        ),
    ]
