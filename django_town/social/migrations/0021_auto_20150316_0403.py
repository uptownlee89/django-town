# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

import django_town.helper.fields


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0020_auto_20150316_0401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='sources',
            field=django_town.helper.fields.JSONField(default='[]'),
            preserve_default=True,
        ),
    ]
