from django.db import models


class AddressComponentType(models.Model):
    name = models.CharField(max_length=50, unique=True)


class AddressComponent(models.Model):
    name = models.CharField(max_length=60)
    parent = models.ForeignKey("AddressComponent", default=None, null=True)
    types = models.ManyToManyField(AddressComponentType)
    depth = models.SmallIntegerField(db_index=True)

    class Meta:
        unique_together = ('parent', 'name')
        app_label = 'social'


class AddressComponentLookup(models.Model):
    locale = models.CharField(max_length=5)
    name = models.CharField(max_length=60)
    component = models.ForeignKey(AddressComponent)

    class Meta:
        unique_together = ('locale', 'component')
        app_label = 'social'


class Country(models.Model):
    ascii_name = models.CharField(max_length=200)
    code = models.CharField(max_length=2, unique=True)

    class Meta:
        app_label = 'social'


class TimeZone(models.Model):
    name = models.CharField(max_length=40, unique=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    gmt = models.FloatField()
    dst = models.FloatField()
    raw = models.FloatField()

    class Meta:
        app_label = 'social'


class GeoName(models.Model):
    ascii_name = models.CharField(max_length=200)
    type = models.CharField(db_index=True, max_length=10)
    lat = models.FloatField()
    lng = models.FloatField()
    country = models.ForeignKey(Country, blank=True, null=True)
    timezone = models.ForeignKey(TimeZone, blank=True, null=True)
    parent = models.ForeignKey('GeoName', blank=True, null=True, default=None)
    pp = models.IntegerField(db_index=True)

    class Meta:
        app_label = 'social'


class _GeoLookup(models.Model):
    geoname = models.ForeignKey(GeoName, unique=True)
    gid = models.IntegerField(unique=True)
    last_modified = models.DateField(db_index=True)

    class Meta:
        app_label = 'social'


class GeoNameLookup(models.Model):
    geoname = models.ForeignKey(GeoName)
    lang = models.CharField(db_index=True, max_length=10)
    name = models.CharField(db_index=True, max_length=200)
    is_preferredName = models.BooleanField(db_index=True, default=False)

    class Meta:
        app_label = 'social'