from django.db import models

from django_town.social.models import User
from django_town.helper.fields import MultiSourceImageField, RandomNameUploadTo, JSONField


POST_STATUS_CHOICES = (
    (0, 'draft'),
    (1, 'private'),
    (2, 'publish'),
)


class Keyword(models.Model):

    name = models.CharField(max_length=30)

    class Meta:
        app_label = 'social'


def _photo_name(photo, file_name):
    return RandomNameUploadTo('photo/')(photo, file_name)


class Photo(models.Model):
    # mime_type = models.CharField(max_length=100)
    keywords = models.ManyToManyField(Keyword)
    photo = MultiSourceImageField(upload_to=_photo_name, width_field="width", height_field="height",
                                  sources_field="sources")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    height = models.IntegerField(default=0, editable=False)
    width = models.IntegerField(default=0, editable=False)
    sources = JSONField(default=[])

    class Meta:
        app_label = 'social'


class Post(models.Model):

    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, db_index=True)
    status = models.SmallIntegerField(db_index=True, choices=POST_STATUS_CHOICES, default=0)
    author = models.ForeignKey(User)
    excerpt = models.TextField(default="")
    content = models.TextField(default="")

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    photos = models.ManyToManyField(Photo)
    keywords = models.ManyToManyField(Keyword)
    thumbnail = models.ForeignKey(Photo, default=None, blank=True, related_name="thumbnail")

    class Meta:
        app_label = 'social'

