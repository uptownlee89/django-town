from django_town.rest.resources import ModelResource, VirtualField
from django_town.rest import RestNotFound
from django_town.social.models.geo import GeoName, GeoNameLookup
from django_town.social.documents.geo import GeoNamePointLookup


class GeoResource(ModelResource):

    def field__name(self, instance):
        return instance.ascii_name

    def field__alter_names(self, instance):
        return {each.lang: each.name for each in GeoNameLookup.objects.filter(geoname=instance._instance).order_by('is_preferredName')}

    # def field__full_name(self, instance):
    #     names = []
    #     names.append(instance._instance)
    #     cur = instance._instance
    #     while cur:
    #         cur = cur.parent
    #         # GeoResource()(cur.parent__id).alter_names
    #     return {}

    name = VirtualField()
    alter_names = VirtualField()
    # full_name = VirtualField()

    class Meta:
        cache_key_format = "_ut_geo:%(pk)s"
        fields = ['id', 'name', 'alter_names', 'lat', 'lng', 'timezone.raw', 'timezone.gmt', 'timezone.dst', 'country.code', 'country.name']
        # cache_ignored_virtual_only = ['liked', 'collected', 'like_count', 'collect_count']
        model = GeoName

default_geo_resource = GeoResource(name='geo')


# Counter({'PPL': 303058, 'ADM4': 48427, 'ADM3': 27280, 'PPLA3': 18432, 'ADMD': 17749, 'ADM2': 17398, 'PPLA2': 10753,
# 'PPLA4': 6879, 'PPLL': 4621, 'PPLX': 4374, 'PPLA': 3389, 'ADM1': 2584, 'PPLC': 238, 'PCLI': 193, 'PPLF': 191,
#  'PPLQ': 112, 'PRSH': 67, 'ADM5': 66, 'PPLR': 58, 'PCLD': 32, 'PPLS': 19, 'PCLIX': 7, 'PPLG': 6, 'PCLS': 5,
# '': 3, 'PCL': 3, 'TERR': 3, 'PCLF': 3, 'STLMT': 1, 'LTER': 1})
# ['', 'PRSH', 'STLMT', 'PCL', 'PPLA2', 'PCLS', 'PPL', 'PCLI', 'ADM4', 'PPLA3', 'TERR', 'PPLA4', 'ADM3', 'PPLA', 'PPLC'
# , 'PPLF', 'PPLG', 'ADMD', 'PPLL', 'PPLQ', 'PPLR', 'PPLS', 'ADM5', 'PPLX', 'LTER', 'ADM1', 'PCLF', 'ADM2', 'PCLD',
#  'PCLIX']
def get_geo_instance(lat, lng):
    geonames = GeoNamePointLookup.objects(__raw__= {
            'location': {
                '$near': {
                    '$geometry': {
                        'type': "Point",
                        'coordinates': [float(lng), float(lat)]
                        },
                    '$maxDistance': 5000
                    }
                },
            'type': {
                '$in': ['PPL', 'ADM4', 'ADM3', 'PPLA3', 'ADM2', 'PPLA2', 'PPLA4', 'PPLL', 'PPLX', 'PPLC']
            }
            })

    try:
        instance = default_geo_resource(geonames[0].geoname_id)
    except IndexError:
        raise RestNotFound(resource_name="geo")
    return instance



from django_town.rest import rest_manager
rest_manager.register_default_resource(default_geo_resource)