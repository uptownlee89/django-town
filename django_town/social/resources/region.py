from django.utils.http import int_to_base36, base36_to_int
from django.db import transaction
from django.db import IntegrityError
from django_town.rest.resources import Resource
from django_town.social.models.geo import AddressComponent, AddressComponentType, AddressComponentLookup


class RegionResource(Resource):

    def create(self, data=None, files=None, acceptable=None, required=None, exclude=None, request=None,
               request_kwargs=None):

        if not data['from_command']:
            raise Exception()
        data, files = self.validate_create(data=data, files=files)
        components = data['components']
        parent = None
        for each in components:
            # types = []
            try:
                _sub_locality = AddressComponent.objects.get(name=each['name'], parent=parent)
            except AddressComponent.DoesNotExist:

                _sub_locality, created = AddressComponent.objects.get_or_create(name=each['name'],
                                                                                parent=parent, depth=each['depth'])
            with transaction.atomic():
                for each_type in each['types']:
                    cur_type, created = AddressComponentType.objects.get_or_create(name=each_type)
                    _sub_locality.types.add(cur_type)
                _sub_locality.save()
            parent = _sub_locality
        region_code = int_to_base36(parent.pk)
        parent = parent.parent
        while parent:
            region_code = int_to_base36(parent.pk) + ":" + region_code
            parent = parent.parent
        return self._meta.resource_instance_cls(region_code, self)

    def update_lang(self, resource_instance, lang, data):

        data, files = self.validate_create(data=data, files=None)

        keys = resource_instance._pk.split(':')
        queryset = AddressComponent.objects

        for index in range(len(keys)):
            queryset = queryset.select_related('parent')
        component = queryset.get(pk=base36_to_int(resource_instance._pk.split(":")[-1]))
        _components = []
        while component.parent:
            _components.insert(0, component)
            component = component.parent
        _components.insert(0, component)
        i = 0
        for each in data['components']:
            try:
                AddressComponentLookup.objects.create(locale=lang, component=_components[i], name=each['name'])
            except IntegrityError:
                pass
            i += 1

    def instance_to_python(self, resource_instance, fields=None, exclude=None, **kwargs):
        keys = resource_instance._pk.split(':')
        components = []
        queryset = AddressComponent.objects

        for index in range(len(keys) - 1):
            queryset = queryset.select_related('parent')
        component = queryset.get(pk=base36_to_int(keys[-1]))
        _components = []
        while component.parent:
            _components.insert(0, component)
            component = component.parent
        _components.insert(0, component)
        for component in _components:
            components.append({'name': component.name, 'region_code': int_to_base36(component.id)})
        ret = {'components': components, 'formatted_name': ' '.join([x['name'] for x in components])}
        ret.update(resource_instance.all_virtual_items(exclude=exclude))
        return ret

    @staticmethod
    def get_name_for_region_id(region_id, lang):
        keys = region_id.split(':')
        k_l = len(keys)
        if k_l < 5:
            keys = region_id.split(':')[k_l-2:k_l-1]
        else:
            keys = region_id.split(':')[k_l-3:k_l-2]
        k_l = len(keys)
        components = []
        queryset = AddressComponent.objects

        for index in range(k_l - 1):
            queryset = queryset.select_related('parent')
        component = queryset.get(pk=base36_to_int(keys[-1]))
        _components = []
        i = 0
        while component.parent and i < k_l - 1:
            _components.insert(0, component)
            component = component.parent
            i += 1
        _components.insert(0, component)
        for component in _components:
            try:
                components.append({'name': AddressComponentLookup.objects.get(component=component, locale=lang).name })
            except AddressComponentLookup.DoesNotExist:
                components.append({'name': component.name })
        ret =' '.join([x['name'] for x in components])
        return ret

    class Meta:
        cache_key_format = "_ut_region:%(pk)s"

default_region_resource = RegionResource()
from django_town.rest import rest_manager
rest_manager.register_default_resource(default_region_resource)