import logging


stats_logger = logging.getLogger('django_town.stats')


def create_object(object_name, object_id):
    stats_logger.info("c_o:%s:%s", object_name, str(object_id))

def create_object_event(object_name, object_id, event_name):
    stats_logger.info("c_o_e:%s:%s:%s", object_name, str(object_id), event_name)

def object_event(object_name, object_id, event_name):
    stats_logger.info("o_e:%s:%s:%s", object_name, str(object_id), event_name)


def object_once_event(object_name, object_id, user_id, event_name):
    stats_logger.info("o_o_e:%s:%s:%s:%s", object_name, str(object_id), str(user_id), event_name)


def _log(type, event_name, **kwargs):
    
    pass