# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django_town.stats.models import Object, Event, EventByUser, EventByDate, EventByHour
import os
import datetime
from django.db import transaction
from django.utils import timezone


class Command(BaseCommand):

    def __init__(self):
        super(Command, self).__init__()
        self.object_cache = {}

    def handle(self, *args, **options):
        info_file = args[0]
        log_path = args[1]
        try:
            f = open(info_file, 'r')
        except FileNotFoundError:
            f = open(info_file, 'w+')
            f.close()

        f = open(info_file, 'r')
        try:
            last_checked, last_modified = f.read().split(',')
        except ValueError:
            last_checked, last_modified = 1, 1

        files = []
        for (dirpath, dirnames, filenames) in os.walk(log_path):
            files.extend(filenames)
            break
        last_checked = float(last_checked)
        last_modified = float(last_modified)
        cur_checked, cur_modified = last_checked, last_modified
        processing_lines = []
        for each in files:
            (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(log_path + each)
            if int(mtime) > last_modified:
                pass

            cur_modified = max(float(cur_modified), float(mtime))

            with open(log_path + each, 'r') as ff:
                for line in ff:
                    created, value = line.split(' ')
                    if float(created) > last_checked:
                        processing_lines.append((created, value.strip()))
                        cur_checked = max(float(cur_checked), float(created))
        f.close()
        with transaction.atomic(using='stats'):
            self.process(processing_lines)


        f = open(info_file, 'w')
        f.write("%f,%f" % (cur_checked, cur_modified))
        f.close()

    def process(self, lines):
        for each in lines:
            created, each_line = each
            action, val = each_line.split(":", 1)
            if action == 'o_o_e':
                obj, obj_key, u_key, event = val.split(':')
                self.process_event(obj, obj_key, event, u_key, created=timezone.make_aware(datetime.datetime.fromtimestamp(float(created)), timezone.get_current_timezone()))
                # self.get_object(obj, obj_key)
            elif action == 'o_e':
                obj, obj_key, event = val.split(':')
                # self.get_object(obj, obj_key)
                self.process_event(obj, obj_key, event, created=timezone.make_aware(datetime.datetime.fromtimestamp(float(created)), timezone.get_current_timezone()))
            elif action == 'c_o':
                obj, obj_key = val.split(':')
                self.get_object(obj, obj_key, timezone.make_aware(datetime.datetime.fromtimestamp(float(created)), timezone.get_current_timezone()))
            elif action == 'c_o_e':
                object_type, object_id, event_name = val.split(':')
                obj = self.get_object(object_type, object_id, created)
                event, __unused = Event.objects.get_or_create(name=event_name, object_name=object_type, object_key=object_id)
                event.object=obj
                event.created = timezone.make_aware(datetime.datetime.fromtimestamp(float(created)), timezone.get_current_timezone())
                EventByDate.objects.get_or_create(event=event, date=datetime.date.today())
                EventByHour.objects.get_or_create(date=datetime.date.today(),
                                                  hour=datetime.datetime.now().hour, event=event)

                event.save()
                # self.process_event(obj, obj_key, event, created=datetime.datetime.fromtimestamp(float(created)))

    def get_object(self, obj_type, obj_id, created):
        key = obj_type + '_' + str(obj_id)
        cached = self.object_cache.get(key)
        if not cached:
            cached, __unused = Object.objects.get_or_create(name=obj_type, object_id=obj_id)
            cached.created=created
            self.object_cache[key] = cached
        return cached

    def process_event(self, object_type, object_id, event_name, once_id=None, created=datetime.datetime.now()):

        obj = self.get_object(object_type, object_id, created)
        event, __unused = Event.objects.get_or_create(name=event_name, object_name=object_type, object_key=object_id)
        event.object=obj
        event.created = created
        event_by_date, __unused = EventByDate.objects.get_or_create(event=event,
                                                                    date=created.date())
        event_by_hour, __unused = EventByHour.objects.get_or_create(date=created.date(),
                                                                   hour=created.hour, event=event)

        if once_id:
            unused, is_first_time = EventByUser.objects.get_or_create(event=event, user_id=once_id)
            if is_first_time:
                event.once += 1
                event_by_date.once += 1
                event_by_hour.once += 1
        else:
            event.count += 1
            event_by_date.count += 1
            event_by_hour.count += 1

        event.save()
        event_by_date.save()
        event_by_hour.save()