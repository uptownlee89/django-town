# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='event',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='event',
            name='object',
        ),
        migrations.AlterUniqueTogether(
            name='eventbydate',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='eventbydate',
            name='event',
        ),
        migrations.DeleteModel(
            name='EventByDate',
        ),
        migrations.AlterUniqueTogether(
            name='eventbyuser',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='eventbyuser',
            name='event',
        ),
        migrations.DeleteModel(
            name='Event',
        ),
        migrations.DeleteModel(
            name='EventByUser',
        ),
        migrations.DeleteModel(
            name='Object',
        ),
    ]
