# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0002_auto_20150214_0009'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=30, db_index=True)),
                ('count', models.IntegerField(default=0, db_index=True)),
                ('once', models.IntegerField(default=0, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventByDate',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('date', models.DateField()),
                ('count', models.IntegerField(default=0, db_index=True)),
                ('once', models.IntegerField(default=0, db_index=True)),
                ('event', models.ForeignKey(to='stats.Event')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventByUser',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('event', models.ForeignKey(to='stats.Event')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Object',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=30, db_index=True)),
                ('object_id', models.CharField(max_length=30, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='object',
            unique_together=set([('name', 'object_id')]),
        ),
        migrations.AlterUniqueTogether(
            name='eventbyuser',
            unique_together=set([('user_id', 'event')]),
        ),
        migrations.AlterUniqueTogether(
            name='eventbydate',
            unique_together=set([('date', 'event')]),
        ),
        migrations.AddField(
            model_name='event',
            name='object',
            field=models.ForeignKey(to='stats.Object'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='event',
            unique_together=set([('name', 'object')]),
        ),
    ]
