# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0009_auto_20150214_0051'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventByHour',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('date', models.DateField()),
                ('hour', models.IntegerField(db_index=True)),
                ('count', models.IntegerField(default=0, db_index=True)),
                ('once', models.IntegerField(default=0, db_index=True)),
                ('event', models.ForeignKey(to='stats.Event')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='eventbyhour',
            unique_together=set([('date', 'hour', 'event')]),
        ),
    ]
