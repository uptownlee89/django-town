# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0010_auto_20150215_0457'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 15, 5, 50, 56, 537389), db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='object',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 15, 5, 51, 1, 993610), db_index=True),
            preserve_default=False,
        ),
    ]
