# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0012_auto_20150215_0635'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='object_key',
            field=models.CharField(max_length=30, default=None),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='event',
            unique_together=set([('name', 'object_name', 'object_key')]),
        ),
    ]
