# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0013_auto_20150215_0639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='object',
            field=models.ForeignKey(null=True, to='stats.Object'),
            preserve_default=True,
        ),
    ]
