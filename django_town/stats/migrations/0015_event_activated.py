# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0014_auto_20150215_0649'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='activated',
            field=models.BooleanField(default=True, db_index=True),
            preserve_default=True,
        ),
    ]
