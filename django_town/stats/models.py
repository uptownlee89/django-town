from django.db import models



class Object(models.Model):
    name = models.CharField(max_length=30, db_index=True)
    object_id = models.CharField(max_length=30, db_index=True)
    created = models.DateTimeField(db_index=True, auto_now_add=True)

    class Meta:
        unique_together = (('name', 'object_id'), )
        app_label = 'stats'


class Event(models.Model):
    name = models.CharField(max_length=30)
    object_name = models.CharField(max_length=30, db_index=True)
    object_key = models.CharField(max_length=30)
    object = models.ForeignKey(Object, null=True)
    count = models.IntegerField(db_index=True, default=0)
    once = models.IntegerField(db_index=True, default=0)
    created = models.DateTimeField(db_index=True, auto_now_add=True)
    activated = models.BooleanField(default=True, db_index=True)

    class Meta:
        unique_together = (('name', 'object_name', 'object_key'), )
        app_label = 'stats'


class EventByUser(models.Model):
    event = models.ForeignKey(Event)
    user_id = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('user_id', 'event'), )
        app_label = 'stats'


class EventByDate(models.Model):
    event = models.ForeignKey(Event)
    date = models.DateField()
    count = models.IntegerField(db_index=True, default=0)
    once = models.IntegerField(db_index=True, default=0)

    class Meta:
        unique_together = (('date', 'event',), )
        app_label = 'stats'


class EventByHour(models.Model):
    event = models.ForeignKey(Event)
    date = models.DateField()
    hour = models.IntegerField(db_index=True)
    count = models.IntegerField(db_index=True, default=0)
    once = models.IntegerField(db_index=True, default=0)

    class Meta:
        unique_together = (('date', 'hour', 'event'), )
        app_label = 'stats'
